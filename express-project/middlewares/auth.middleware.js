const isAuthorized = (req, res, next) => {
    if(req && req.headers && req.headers.authorization && req.headers.authorization == 'admin') {
        // console.log(req.headers);
        next();
    } else {
        res.status(401).send();
    }

};

module.exports = {
    isAuthorized
}