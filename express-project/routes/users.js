let express = require('express');
const fs = require("fs");

let router = express.Router();

const { saveName} = require("../services/user.service");
const {isAuthorized} = require('../middlewares/auth.middleware');

/* GET users listing. */
router.get('/', function(req, res, next) {
  fs.readFile('./user.json', "utf8", 
            function(error,data){
                if(error) {
                  res.status(400).send("Some Error"); 
                } else {
                  let arrUsers = [];
                  JSON.parse(data).forEach(element => {
                    arrUsers.push(element.name);
                  });
                  res.send(arrUsers);
                }
    });
});



router.get('/:id', function(req, res, next) {

  fs.readFile('./user.json', "utf8", 
  function(error,data){
      if(error) {
        res.status(400).send("Some Error"); 
      } else {
        let arr = JSON.parse(data);
        arr.forEach((element, index, array) => {
          if(element._id == req.params.id)  {
             res.send(element);
          } 
        }); 

        if(req.params.id > arr.length || req.params.id < 1) {
           res.send("no such user");
        }

      }
  });
});


router.delete('/:id', function(req, res, next) {  
  fs.readFile('./user.json', "utf8", 
  function(error,data){
      if(error) {
        res.status(400).send("Some Error"); 
      } else {
        let arr = JSON.parse(data);
        let flag = 0;
        arr.forEach((element, index, array) => {
          if(element._id == req.params.id)  {
             arr.splice(index, 1);
             flag = 1;
          }
        });

        if(flag) {
          let information = JSON.stringify(arr);
          fs.writeFile("./user.json", information, (err) => {
            if (err) throw err;
            console.log('Data written to file');
            res.send("ok");
          });
        } else {
          res.send("no such user or maybe you have already delete it");
        }
         
      } 
  });
});


router.put('/:id', function(req, res, next) {  
  fs.readFile('./user.json', "utf8", 
  function(error, data){
      if(error) {
        res.status(400).send("Some Error"); 
      } else {
        let arr = JSON.parse(data);
        let user;
        let flag = 0;
        let index;
        arr.forEach((element, ind, array) => {
          if(element._id == req.params.id)  {
             user = element;
             index = ind;
             flag = 1;
          }
        });

        if(flag) {
          Object.assign(arr[index], req.body);
          let information = JSON.stringify(arr);
          fs.writeFile("./user.json", information, (err) => {
            if (err) req.send("something error");
            else res.send("updated successfully");
          });
        } else {
          res.send("no such user sorrrrrrrrryyyyy");
        }         
      } 
  });
});


router.post('/', isAuthorized, function(req, res, next) {
  const result = saveName(req.body);
  if(result) {
    res.send(`your name is ${result}`);
  } else {
    res.status(400).send('some error');
  }

});
module.exports = router;
